#! /bin/bash

# Sample start up scripts
sudo timedatectl set-timezone ${timezone}
sudo hostnamectl set-hostname ${hostname}
sudo useradd -s /bin/bash -d /home/${new_user}/ -m -G sudo ${new_user}

              